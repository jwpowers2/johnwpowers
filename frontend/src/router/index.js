import Vue from "vue";
import VueRouter from "vue-router";
import Home from "../views/Home.vue";
import Experience from "@/components/Experience.vue";
import Education from "@/components/Education.vue";
import Conversion from "@/components/Conversion.vue";
import Recurse from "@/components/Recurse.vue";
import BodyGreeting from "@/components/BodyGreeting.vue";
import Body from "@/components/Body.vue";
import Header from "@/components/Header.vue";
import FAQ from "@/views/FAQ.vue";

Vue.use(VueRouter);

const routes = [
  {
    path: "/",
    name: "Home",
    component: Home,
    children: [
      {
        path: "faq",
        component: FAQ
      },
      {
        path: "body",
        name: "body",
        component: Body,
        children: [
          {
            path: "bodygreeting",
            name: "bodygreeting",
            component: BodyGreeting
          },
          {
            path: "experience",
            name: "experience",
            component: Experience
          },
          {
            path: "education",
            name: "education",
            component: Education
          },
          {
            path: "conversion",
            name: "conversion",
            component: Conversion
          },
          {
            path: "recurse",
            name: "recurse",
            component: Recurse
          }
        ]
      }
    ]
  }
];

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes
});

export default router;
